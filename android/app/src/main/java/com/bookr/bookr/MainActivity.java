package com.bookr.bookr;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.view.View;

import com.facebook.react.ReactActivity;
import android.content.Intent;
import com.facebook.react.modules.i18nmanager.I18nUtil;

public class MainActivity extends ReactActivity {
    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */

    @Override
    public void onNewIntent (Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }


    @Override
    protected String getMainComponentName() {
        return "letsbookr";
    }
}
