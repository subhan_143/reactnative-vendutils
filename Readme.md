# Bookr

Salon customer mobile application

### Prerequisites

Node
React native > 0.40
Android Studio
Xcode

### Installation

Setup and Installation:

1. Install Node and npm (npm v4.0.0)
2. Clone the project.
3. Run (npm install)
4. Install all the dependies mentioned in package.json file.
4. Setup environment by changing urls and environment variable in the constant.js file in the app folder.
5. Run react-native run-android or react-native run-ios to run the project on simulator/device.
