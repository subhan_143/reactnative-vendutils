import DeviceInfo from 'react-native-device-info'
import { Platform } from "react-native";


const QABundleIDiOS = "com.zainabookr.QA"
const DEVBundleIDiOS = "com.zainabookr.DEV"
const PRODBundleIDiOS = "com.zainabookr"
// ------------------
const QABundleIDAndroid = "com.bookr.bookr.QA"
const DEVBundleIDAndroid = "com.bookr.bookr.DEV"
const PRODBundleIDAndroid = "com.bookr.bookr"

 //base url
export let baseUrl = "" 


//  export const baseUrl = "http://192.168.200.108:8000/v1/app/"//local 
  
if (Platform.OS === "ios") {
  if (DeviceInfo.getBundleId() === QABundleIDiOS) {
    baseUrl = "https://qa-api.letsbookr.com/v1/app/" //QA server
  }
  else if (DeviceInfo.getBundleId() === DEVBundleIDiOS) {
    baseUrl = "https://dev-api.letsbookr.com/v1/app/" //DEV server
  }
  else if (DeviceInfo.getBundleId() === PRODBundleIDiOS) {
    baseUrl = "https://client-api.letsbookr.com/v1/app/"  //PROD server
  }
}
else {
  if (DeviceInfo.getBundleId() === QABundleIDAndroid) {
    baseUrl = "https://qa-api.letsbookr.com/v1/app/" //QA server
  }
  else if (DeviceInfo.getBundleId() === DEVBundleIDAndroid) {
    baseUrl = "https://dev-api.letsbookr.com/v1/app/" //DEV server
  }
  else if (DeviceInfo.getBundleId() === PRODBundleIDAndroid) {
    baseUrl = "https://client-api.letsbookr.com/v1/app/"  //PROD server
  }
}



console.log(DeviceInfo.getBundleId())
console.log(baseUrl)

 export const timeZone = "Asia/Kuwait";

 export const country = {
  Saudia: 191,
  Kuwait: 115,
};

export const languageEnum = {
  arb: "arb",
  eng: "en",
};

export const cityEnum = {
  Riyadh: 1,
  Jeddah: 2,
};

//--Store link

export const appStoreLink = 'https://itunes.apple.com/us/app/apple-store/id984497050?mt=8'
export const playStoreLink = 'https://play.google.com/store/apps/details?id=com.bookr.bookr'

/// ------------ Storage Keys

export const updatePromptShownDateKey = 'UpdatePromptShownDateKey'