import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, coralred } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: apptheme,
    flexDirection: "column"
  },
  logoSection: {
    flex: 0.4,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  logoImg: {
    height: 111,
    width: 213,
    alignSelf: "center"
  },
  languageSection: {
    flex: 0.6,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50
  },
  titleText: {
    fontSize: 18,
    color: "white"
  },
  buttonStyle: {
    height: 50,
    width: 213,
    marginTop: 20,
    backgroundColor: coralred,
    borderColor: coralred,
    borderRadius: 2,
    justifyContent: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 18,
    alignSelf: "center"
  }
});
