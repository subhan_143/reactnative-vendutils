import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: apptheme,
    flexDirection: "column"
  },
  logoSection: {
    flex: 0.4,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  logoImg: {
    height: 111,
    width: 213,
    alignSelf: "center"
  },
  languageSection: {
    flex: 0.6,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50
  },
  titleText: {
    fontSize: 18,
    color: "white"
  },
  buttonStyle: {
    height: 50,
    width: 213,
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: "#EB7F74",
    borderColor: "#EB7F74",
    borderRadius: 2,
    justifyContent: "center"
  },
  buttonText: {
    color: "white",
    fontSize: 20,
    alignSelf: "center"
  }
});
