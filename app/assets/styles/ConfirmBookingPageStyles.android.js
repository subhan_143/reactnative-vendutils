import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, black } from "../../color";

export default StyleSheet.create({
	  container: {
      flex: 1,
      backgroundColor: white,  
      // width: Dimensions.get('window').width,
    },
    footer: {
      backgroundColor: apptheme,
      justifyContent: 'center',
      height: 50,
    },
    cardview: {
      marginTop: 4,
      marginBottom: 4,
      marginLeft:10,
      marginRight:10,
      paddingHorizontal: 10,
      paddingVertical: 10,
      backgroundColor: white,
    },
    cardrow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    infocontainer:{
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10, 
      height: 140,
      marginTop : 10,
      marginRight :10 ,
      marginLeft:10,
      backgroundColor: white,
      paddingTop: 20,
      marginBottom: 0,

      borderLeftWidth: 3,
      borderBottomWidth: 0,
      borderRightWidth: 3,
      borderTopWidth: 3,
      borderColor: "#e7e7e7"
    },
    textinput:{
      color: '#565656',
      alignSelf: 'stretch',
      fontSize: 12,
      height: 34, 
      backgroundColor : white,
      textAlign: 'center',
      borderColor: '#979797',
      borderWidth: 0.5,

      marginBottom: 10,
      fontFamily: 'Quicksand-Regular', 
    },
    googleAddressBox: {
      height: 34, 
      width : 280,
      backgroundColor : white,
      borderColor: '#979797',
      borderWidth: 0.5,
      marginLeft: 20,
      marginRight:20,
      marginBottom: 10,
      justifyContent: 'center',
      paddingHorizontal: 5,
    },
    completeAddress: {
      color: '#565656',
      alignSelf: 'stretch',
      fontSize: 12,
      height: 34, 
      backgroundColor : white,
      textAlign: 'center',
      borderColor: '#979797',
      borderWidth: 0.5,
      marginBottom: 10,
      fontFamily: 'Quicksand-Regular', 
    },
    currentAddress: {
      color: '#565656',      
      fontSize: 12,
      textAlign: 'center',
      fontFamily: 'Quicksand-Regular',       
    },
    loader:{
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      width : Dimensions.get('window').width,
      height : Dimensions.get('window').height,
      opacity: 0.4,
      backgroundColor: 'black',   
    },
    addAnotherServiceSection: {
      // flex :0.7 , 
      marginLeft:10, 
      marginRight:10, 
      marginBottom: 150,
      justifyContent: 'flex-start',
    },
    addAnotherServiceText: {
      textAlign:'center', 
      color: white, 
      margin: 4, 
      fontSize: 14, 
      fontFamily: 'Quicksand-Bold',
    },
    listViewSection: {
      flex :0.7 , 
      marginTop: 10, 
      marginLeft:5,
      marginRight:5, 
      justifyContent: 'flex-start'
    },
    serviceNameSection: {
      marginTop: 2,
    },
    serviceName: {
      flex: 0.9
    },
    serviceNameText: {
      fontSize: 14 ,
      fontFamily: 'Quicksand-Bold',
      color: apptheme, 
      textAlign: 'left'
    },
    cancel: {
      flex: 0.1,
      alignItems: 'flex-start',
    },
    borderSection: {
      borderBottomColor: '#E7E7E7',
      borderBottomWidth: 2,
      marginRight: 50,
    },
    scheduleSection: {
      flexDirection : 'row',
      marginTop: 2 
    },
    salonNameSection: {
       flexDirection : 'row',
       marginTop: 2
    },
    salonNameText: {
       fontSize: 13 , 
       color: '#888888', 
       fontFamily: 'Quicksand-Bold'
    },
    providerNameSection: {
      marginBottom: 15,
      justifyContent: 'space-between',
      flexDirection : 'row', 
      marginTop: 2, 
    },
    providerNameText: {
       fontSize: 13 , 
        color: '#888888', 
        marginLeft: 0,
        fontFamily: 'Quicksand-Bold'
    },
    viewMoreNameText: {
      fontSize: 13 , 
        color: apptheme, 
        marginLeft: 0,
        fontFamily: 'Quicksand-Bold'
    },
    text: {
      fontSize: 13 , color : '#888888',fontFamily: 'Quicksand-Bold' 
    },
    bookItBtnText: {
      textAlign:'center',
      color: white,
      margin: 4, 
      fontSize: 16, 
      fontFamily: 'Quicksand-Bold'
    },
    currencyNameText: {
      fontSize: 14,
      fontFamily: "Quicksand-Bold",
      color: "#565656"
    },
    currencyValueText: {
      fontSize: 14,
      fontFamily: "Quicksand-Bold",
      color: "#565656"
    }

  });
