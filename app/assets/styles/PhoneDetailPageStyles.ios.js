import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, coralred } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: apptheme,
    flexDirection: "column"
  },
  logoSection: {
    flex: 0.5,
    justifyContent: "center",
    alignItems: "center"
  },
  logoImage: {
    height: 128,
    width: 128
  },
  titleSection: {
    marginHorizontal: 35,
    marginTop: 30
  },
  titleText: {
    textAlign: "center",
    fontSize: 18,
    color: "white"
  },
  inputSection: {
    flex: 0.5,
    justifyContent: "flex-start",
    alignItems: "center",
    marginTop: 10
  },
  nextBtn: {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: "100%",
    justifyContent: "center",
    height: 45
  },
  nextBtnText: {
    color: "white",
    fontSize: 18,
    alignSelf: "center",
    fontFamily: "Quicksand-Bold"
  },
  loader: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    backgroundColor: apptheme
  },
  subloader: {
    marginTop: 25,
    flexDirection: "row",
    justifyContent: "center"
  }
});
