import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white } from "../../color";
import { Platform } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1
  },
  bookingcontainer: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 5,
    backgroundColor: white
  },
  calendercontainer: {
    height: Platform.isPad ? 160 : 75,
    marginBottom: 20,
    paddingRight: 10,
    paddingLeft: 10
  },
  textcontainer: {
    color: apptheme,
    fontFamily: "Quicksand-Bold",
    marginBottom: 5,
    textAlign: "left"
  }
});
