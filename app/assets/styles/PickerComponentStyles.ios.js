import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white } from "../../color";

export default StyleSheet.create({
  container: {
    flexDirection: "column"
  },
  pickerStyle: {
    width: 310,
    height: 40,
    backgroundColor: white,
    borderColor: "#979797",
    borderWidth: 1,
    alignSelf: "center",
    justifyContent: "center"
  },
  countrytext: {
    textAlign: "center",
    fontSize: 16
  },
  items: {
    color: "#e7e7e7",
    textAlign: "center"
  },
  phoneSection: {
    flexDirection: "row",
    marginHorizontal: 50,
    marginTop: 10
  },
  regionSection: {
    alignItems: "flex-start"
  },
  textSection: {
    height: 40,
    width: 78,
    justifyContent: "center",
    backgroundColor: white,
    borderColor: "#979797",
    borderWidth: 1,
    marginRight: 5
  },
  regionInput: {
    alignSelf: "center",
    fontFamily: "Quicksand-Light",
    color: "#000000",
    fontSize: 16
  },
  phoneInputSection: {
    alignItems: "flex-end"
  },
  phoneInput: {
    width: 223,
    height: 40,
    textAlign: "center",
    borderColor: "#979797",
    backgroundColor: white,
    borderWidth: 1,
    marginLeft: 5,
    fontFamily: "Quicksand-Light",
    fontSize: 16
  }
});
