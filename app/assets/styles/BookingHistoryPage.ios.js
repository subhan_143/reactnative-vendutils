import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, black, coralred, lightgray } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFFFFF"
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 50,
    backgroundColor: "#E7E7E7"
  },
  headerText: {
    fontSize: 18,
    textAlign: "center"
  },
  subHeaderText: {
    fontSize: 11,
    color: apptheme,
    textAlign: "center"
  },
  giftButton: {
    padding: 5,
    marginRight: 15,
    backgroundColor: coralred,
    borderColor: coralred,
    borderRadius: 2,
    alignSelf: "center"
  },
  giftButtonText: {
    color: "white",
    fontFamily: "Quicksand-Bold",
    fontSize: 12
  },
  navPillContainer: {
    height: 45,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white"
  },

  navPillPreviousButtonNonActive: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: "#A6A6A6",
    borderRadius: 0,
    paddingVertical: 5,
    paddingHorizontal: 15,
    backgroundColor: "white"
  },
  navPillButtonTextNonActive: {
    color: "#A6A6A6",
    textAlign: "center",
    fontFamily: "Quicksand-Bold"
  },
  navPillPreviousButtonActive: {
    borderWidth: 1,
    borderLeftWidth: 0,
    borderColor: "#A6A6A6",
    borderRadius: 0,
    paddingVertical: 5,
    paddingHorizontal: 15,
    backgroundColor: "#A6A6A6"
  },
  navPillPreviousTextActive: {
    color: "white",
    textAlign: "center",
    fontFamily: "Quicksand-Bold"
  },
  navPillCurrentButtonNonActive: {
    borderWidth: 1,
    borderRightWidth: 0,
    borderColor: "#A6A6A6",
    borderRadius: 0,
    paddingVertical: 5,
    paddingHorizontal: 15,
    backgroundColor: "white"
  },
  navPillCurrentTextNonActive: {
    color: "#A6A6A6",
    textAlign: "center",
    fontFamily: "Quicksand-Bold"
  },
  navPillCurrentButtonActive: {
    borderWidth: 1,
    borderRightWidth: 0,
    borderColor: "#A6A6A6",
    borderRadius: 0,
    paddingVertical: 5,
    paddingHorizontal: 15,
    backgroundColor: "#A6A6A6"
  },
  navPillCurrentTextActive: {
    color: "white",
    textAlign: "center",
    fontFamily: "Quicksand-Bold"
  },
  bookingHistoryContainer: {
    backgroundColor: "#F6F6F6",
    flex: 1,
    marginTop: 3
  },
  cardViewContainer: {
    //height: 80,
    margin: 4,
    justifyContent: "space-between",
    backgroundColor: "white",
    flexDirection: "row",
    flex: 1
  },
  telephoneContainer: {
    alignSelf: "center",
    marginHorizontal: 8
  },
  contentContainer: {
    flex: 1,
    borderRightWidth: 1,
    borderLeftWidth: 1,
    paddingTop: 5,
    paddingBottom: 5,
    marginTop: 15,
    marginBottom: 15,
    borderColor: "#A6A6A6",
    backgroundColor: "transparent",
    flexDirection: "column"
  },
  cancelButtonContainer: {
    alignSelf: "center",
    marginHorizontal: 8,
    paddingRight: 3,
    paddingBottom: 5
  },
  serviceHeading: {
    flex: 0.5,
    justifyContent: "center",
    paddingLeft: 5,
    paddingTop: 5,
    paddingRight: 3,
  },
  serviceHeadingText: {
    fontSize: 13,
    color: "black",
    fontFamily: "Quicksand-Bold",
    textAlign: "left"
  },
  serviceDetails: {
    flex: 0.5,
    justifyContent: "center",
    marginBottom: 5,
    marginTop: 8
  },
  serviceDetailsText: {
    color: "black",
    fontFamily: "Quicksand-Bold",
    fontSize: 10,
    marginLeft: 2,
    textAlign: "left",
    alignSelf: "flex-start",
    //marginTop: 2,
    paddingBottom: 3
  },
  locTime: {
    flexDirection: "row",
    justifyContent: 'space-between',
  },
  locationStyle: {
    flexDirection: "row",
    marginLeft: 3,
    paddingRight: 3,    
    flex: 0.37
  },
  calenderStyle: {
    flexDirection: "row",
    paddingRight: 3,
    marginLeft: 3,
    flex: 0.33
  },
  clockStyle: {
    flexDirection: "row",
    paddingRight: 3,
    marginLeft: 3,
    flex: 0.30
  },
  serviceButtonContainer: {
    alignSelf: "center"
  },
  serviceButton: {
    height: 58,
    width: 58,
    justifyContent: "center"
  },
  progressView: {
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    opacity: 0.4,
    backgroundColor: black
  },
  circle: {
    width: 46,
    height: 46,
    borderRadius: 23,
    justifyContent: "center"
  },
  labelView: {
    marginTop: 4,
    alignSelf: "center",
    color: coralred
  },
  viewMoreText: {
    fontSize: 12,    
    color: apptheme,
    fontFamily: "Quicksand-Bold"
  },
  alertSection: {
   // justifyContent: "flex-start",
   // alignItems: "flex-start",
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 5
  },
  noBookingsMessageView: {
    position: 'absolute', 
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
    

  },
  noBookingsMessageLabel: {
    
    color:lightgray,
  
  }
});
