import { StyleSheet, Dimensions, I18nManager } from "react-native";
import { apptheme, white, coralred } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  },
  listitem: {
    flexDirection: "row"
  },
  listStyle: {
    flexDirection: I18nManager.isRTL ? "row-reverse" : "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    justifyContent: "flex-start",
    alignContent: "center",
    alignSelf: "center"
  },
  circleStyleUnselected: {
    backgroundColor: apptheme,
    borderRadius: 30,
    height: 60,
    width: 60,
    justifyContent: "center",
    margin: 5
  },
  circleStyleSelected: {
    backgroundColor: coralred,
    borderRadius: 30,
    height: 60,
    width: 60,
    justifyContent: "center",
    margin: 5
  },
  itemSlotText: {
    textAlign: "center",
    color: white,
    fontSize: 14,
    fontWeight: "400"
  },
  employeeSection: {
    flexDirection: "column"
  },
  employeeSectionBorder: {
    borderBottomWidth: 0.5,
    borderColor: "#E7E7E7"
  },
  employeeNameText: {
    width: Dimensions.get("window").width,
    padding: 10,
    textAlign: "center",
    fontSize: 18,
    color: "rgb(233, 12, 71)"
  }
});
