import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, coralred, black } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  header: {
    padding: 12,
    backgroundColor: "#E7E7E7",
    justifyContent: "space-between",
    flexDirection: "row",
    alignItems: "center"
  },
  footer: {
    backgroundColor: coralred,
    justifyContent: "center",
    height: 55
  },
  footerText: {
    color: white,
    textAlign: "center",
    fontFamily: "Quicksand-Bold",
    fontSize: 16
  },
  loader: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    opacity: 0.4,
    backgroundColor: "black"
  },
  prevBtn: {
    flex: 0.1,
    alignItems: "center",
    backgroundColor: coralred,
    borderRadius: 5,
    padding: 4
  },
  prevBtnTxt: {
    fontSize: 14,
    color: white
  },
  prevContainer: {
    flex: 0.1,
    alignItems: "center"
  },
  headingView: {
    flex: 0.8,
    alignItems: "center"
  },
  headingText: {
    color: coralred,
    fontFamily: "Quicksand-Bold",
    fontSize: 15,
    textAlign: "center"
  },
  nextBtn: {
    flex: 0.1,
    alignItems: "center",
    backgroundColor: coralred,
    borderRadius: 5,
    padding: 4
  },
  nextBtnTxt: {
    fontSize: 14,
    color: white
  },
  nextContainer: {
    flex: 0.1,
    alignItems: "center"
  },
  calendarComponentView: {
    height: 150,
    marginBottom: 20,
  },
  timeSlotPagerView: {
    flex: 1
  }
});
