import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, lightgray } from "../../color";

export default StyleSheet.create({
	 container: {
      flex: 1,
      backgroundColor: white,  
    },
    footer: {
      backgroundColor: apptheme,
      justifyContent: 'center',
      height: 50,
    },
    cardview: {
      marginTop: 4,
      marginBottom: 4,
      marginLeft:8,
      marginRight:8,
      paddingHorizontal: 8,
      paddingVertical: 8,
      backgroundColor: white,
    },
    cardrow: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    infocontainer:{
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 10,
      paddingTop:20,
      marginTop : 5,
      marginRight :10 ,
      marginLeft:10,
      height: 140,
      backgroundColor: white,
      shadowOpacity: 0.50,
      shadowRadius: 4,
      shadowColor: lightgray,
      shadowOffset: { height: 0, width: 0 },
      
    },
    textinput:{
      alignSelf: 'stretch',
      color: '#565656',
      fontSize: 12,
      height: 33, 
      backgroundColor : white,
      textAlign: 'center',
      borderColor: '#979797',
      borderWidth: 1,
      marginBottom: 10,
      fontFamily: 'Quicksand-Regular',       
    },
    googleAddressBox: {
      height: 33, 
      width : 280,
      backgroundColor : white,
      borderColor: '#979797',
      borderWidth: 1,
      marginLeft: 20,
      marginRight:20,
      marginBottom: 10,
      justifyContent: 'center',
      paddingHorizontal: 5,
    },
    completeAddress: {
      alignSelf: 'stretch',
      color: '#565656',
      fontSize: 12,
      height: 33, 
      backgroundColor : white,
      textAlign: 'center',
      borderColor: '#979797',
      borderWidth: 1,
      marginBottom: 10,
      fontFamily: 'Quicksand-Regular', 
    },
    currentAddress: {
      color: '#565656',      
      fontSize: 12,
      textAlign: 'center',
      fontFamily: 'Quicksand-Regular',       
    },
    loader:{
      position: 'absolute',
      justifyContent: 'center',
      alignItems: 'center',
      width : Dimensions.get('window').width,
      height : Dimensions.get('window').height,
      opacity: 0.4,
      backgroundColor: 'black', 
        
    },
    addAnotherServiceSection: {
      // flex :0.6 , 
      marginLeft:20, 
      marginRight:20, 
      marginBottom:150,
      justifyContent: 'flex-start'
    },
    addAnotherServiceText: {
      textAlign:'center', 
      color: white, 
      margin: 4, 
      fontSize: 14, 
      alignSelf: 'center',
      fontFamily: 'Quicksand-Bold',
    },
    listViewSection: {
      flex :0.7 , 
      marginTop: 10, 
      justifyContent: 'flex-start'
    },
    serviceNameSection: {
      marginTop: 2,
    },
    serviceName: {
      flex: 0.9
    },
    serviceNameText: {
      fontSize: 14 ,
      fontFamily: 'Quicksand-Bold',
      color: apptheme, 
      textAlign: 'left'      
    },
    cancel: {
      flex: 0.1,
      alignItems: 'center',
    },
    borderSection: {
      borderBottomColor: '#E7E7E7',
      borderBottomWidth: 2,
      marginRight: 50,
    },
    scheduleSection: {
      flexDirection : 'row',
      marginTop: 2,
    },
    salonNameSection: {
       flexDirection : 'row',
       marginTop: 2
    },
    salonNameText: {
       fontSize: 13 , 
       color: '#888888', 
       fontFamily: 'Quicksand-Bold'
    },
    providerNameSection: {
      marginBottom: 5,
      justifyContent: 'space-between',
      flexDirection : 'row', 
      marginTop: 2, 
    },
    viewMoreNameText: {
      fontSize: 13 , 
      color: apptheme, 
      marginLeft: 0,
      fontFamily: 'Quicksand-Bold'
    },
    providerNameText: {
      fontSize: 13 , 
      color: '#888888', 
      fontFamily: 'Quicksand-Bold'
    },
    text: {
      fontSize: 13 , color : '#888888',fontFamily: 'Quicksand-Bold' 
    },
    bookItBtnText: {
      textAlign:'center',
      color: white,
      margin: 4, 
      fontSize: 16, 
      fontFamily: 'Quicksand-Bold'
    },
    currencyNameText: {
      fontSize: 14,
      fontFamily: "Quicksand-Bold",
      color: "#565656"
    },
    currencyValueText: {
      fontSize: 14,
      fontFamily: "Quicksand-Bold",
      color: "#565656"
    }
  
});
