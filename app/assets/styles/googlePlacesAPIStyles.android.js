import {
  StyleSheet,
  Dimensions,
} from 'react-native';
import { apptheme, white } from '../../color';

export default StyleSheet.create({
	 textInputContainer: {
          width : Dimensions.get('window').width,
        },
        description: {
          fontWeight: 'bold'
        },
        predefinedPlacesDescription: {
          color: '#1faadb'
        }

});
