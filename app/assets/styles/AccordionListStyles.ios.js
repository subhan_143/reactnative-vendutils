import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, black, coralred } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: white
  },
  header: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  headerText: {
    color: apptheme,
    fontFamily: "Quicksand-Bold",
    marginRight: 10
  },
  headerTextOffer: {
    color: apptheme,
    fontFamily: "Quicksand-Bold",
    marginRight: 12
  },
  listview: {},
  cardview: {
    margin: 3,
    paddingHorizontal: 15,
    paddingVertical: 0,
    backgroundColor: white
  },
  cardrow: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  cardrowOffers: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  titleCheckboxSection: {
    marginTop: 5
  },
  titleSection: {
    flex: 0.8
  },
  titleText: {
    fontSize: 14,
    fontFamily: "Quicksand-Bold",
    color: "#565656",
    textAlign: "left"
  },
  titleTextOffer: {
    fontSize: 14,
    fontFamily: "Quicksand-Bold",
    color: black,
    textAlign: "left"
  },
  checkBoxSection: {
    flex: 0.1,
    alignItems: "center"
  },
  durationText: {
    fontSize: 12,
    color: "#b6b6b6",
    fontFamily: "Quicksand-Bold"
  },
  extraTimeRequiredText: {
    fontSize: 12,
    marginRight: "auto",
    marginLeft: 10,
    color: "#b6b6b6",
    fontFamily: "Quicksand-Bold"
  },
  viewMoreSection: {
    marginTop: 8,
    marginBottom: 5
  },
  viewMoreText: {
    color: apptheme,
    fontSize: 10,
    fontFamily: "Quicksand-Bold"
  },
  currencyNameText: {
    fontSize: 14,
    fontFamily: "Quicksand-Bold",
    color: "#565656"
  },
  currencyNameTextOffer: {
    fontSize: 14,
    fontFamily: "Quicksand-Bold",
    color: black
  }
});
