import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white } from "../../color";

export default StyleSheet.create({
  container: {
    flexDirection: "column"
  },
  pickerStyle: {
    width: 310,
    height: 40,
    backgroundColor: white,
    borderColor: "#979797",
    borderWidth: 0.5,
    alignSelf: "center",
    justifyContent: "center"
  },
  items: {
    color: "#e7e7e7",
    textAlign: "center"
  },
  phoneSection: {
    flexDirection: "row",
    marginHorizontal: 50,
    marginTop: 10
  },
  regionSection: {
    alignItems: "flex-start"
  },
  textSection: {
    height: 40,
    width: 78,
    justifyContent: "center",
    backgroundColor: white,
    borderColor: "#979797",
    borderWidth: 0.5,
    marginRight: 5
  },
  regionInput: {
    alignSelf: "center",
    fontFamily: "Quicksand-Bold",
    color: "#000000"
  },
  phoneInputSection: {
    alignItems: "flex-end"
  },
  phoneInput: {
    width: 223,
    height: 40,
    textAlign: "center",
    borderColor: "#979797",
    backgroundColor: white,
    borderWidth: 0.5,
    marginLeft: 5
  }
});
