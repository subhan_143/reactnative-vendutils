import { StyleSheet, Dimensions } from "react-native";
import { apptheme, white, black } from "../../color";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: apptheme
  },
  profilePicSection: {
    justifyContent: "center",
    alignItems: "center",
    flex: 0.2,
    marginTop: 20
  },
  textFieldInputAndSwitchSection: {
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 0.7,
    flexDirection: "column",
    marginTop: 20
  },
  inputFields: {
    height: 50,
    flexDirection: "row",
    backgroundColor: white,
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "black"
  },
  inputHeaders: {
    width: 100,
    textAlign: "left",
    fontSize: 16,
    paddingLeft: 5
  },
  textcontainer: {
    borderWidth: 1,
    borderColor: "black"
  },
  textinput: {
    width: 210,
    textAlign: "left",
    padding: 5
  },
  textinputRTL: {
    width: 210,
    textAlign: "right",
    padding: 5
  },
  segmentSection: {
    width: 300,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 8
  },
  segmentHeaderText: {
    width: 90,
    color: white,
    fontSize: 16,
    textAlign: "left"
  },
  tabsContainerStyle: {
    width: 200
  },
  tabStyle: {
    //custom styles
    backgroundColor: apptheme,
    borderColor: white
  },
  tabTextStyle: {
    //custom styles
    color: white
  },
  activeTabStyle: {
    //custom styles
    backgroundColor: white,
    borderColor: white
  },
  activeTabTextStyle: {
    //custom styles
    color: apptheme
  },
  updateProfileButtonSection: {
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 0.1,
    marginBottom: 50
  },
  updateButton: {
    borderRadius: 100,
    borderColor: white,
    width: 300,
    height: 45,
    borderWidth: 1,
    justifyContent: "center"
  },
  updateButtonText: {
    fontSize: 18,
    color: white,
    alignSelf: "center"
  },
  loader: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    opacity: 0.4,
    backgroundColor: "black"
  },
  imageloader: {
    position: "absolute",
    width: 100,
    height: 100,
    borderRadius: 50,
    justifyContent: "center",
    alignItems: "center",
    opacity: 0.4,
    backgroundColor: "black"
  },
  thumbnail: {
    width: 100,
    height: 100,
    borderRadius: 50,
    borderWidth: 0.5,
    borderColor: white
  },
  custom: {
    borderBottomWidth: 0
  }
});
