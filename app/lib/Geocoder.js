import Geocoder from 'react-native-geocoding';
 
export default function () {
Geocoder.setApiKey('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'); // use a valid API key 
 
Geocoder.getFromLocation("Colosseum").then(
      json => {
        var location = json.results[0].geometry.location;
        alert(location.lat + ", " + location.lng);
      },
      error => {
        alert(error);
      }
    );
 
/*Geocoder.getFromLatLng(41.89, 12.49).then(
      json => {
        var address_component = json.results[0].address_components[0];
        alert(address_component.long_name);
      },
      error => {
        alert(error);
      }
    );*/
}