import {Alert} from 'react-native';
import {locale} from '../containers/AppContainer';

export function loginvalidation (useremail, userpassword, fromLogin){
    var emailFlag = false , passFlag = true, noSpace = false;
    var inValid = /\s/;
    noSpace = inValid.test(userpassword);
    if(useremail == "" || userpassword == ""){
      if(fromLogin)
        Alert.alert('', locale.t('enterEmailPasswordSignIn'), [{text: locale.t('ok')}]);
      else
        Alert.alert('', locale.t('enterEmailPassword'), [{text: locale.t('ok')}]);      
      return false;
    }

    else if(useremail.length >0){
      var validator = require("email-validator");
      emailFlag = validator.validate(useremail);      
      
      if(userpassword.length < 5){
        passFlag = false;
      }
      if(noSpace) {
        Alert.alert('', locale.t('passwordMustNotContainSpaces'), [{text: locale.t('ok')}]);
        return false;
      }

      if(emailFlag && passFlag){
        return true;
      }

      else{
      
        if(!emailFlag){
          Alert.alert('', locale.t('enterValidEmail'), [{text: locale.t('ok')}]);
          return false;
        }
        else if(!passFlag){
          Alert.alert('', locale.t('passwordsMustHaveAtleastCharacters'), [{text: locale.t('ok')}]);
          return false;
        }
        
      }
    }
}

export function emailValidation(useremail){
  var emailFlag = false
  if(useremail == ""){
    Alert.alert('', locale.t('enterEmailAddress'), [{text: locale.t('ok')}]);
    return false;
  }

  else if(useremail.length >0){
    var validator = require("email-validator");
    emailFlag = validator.validate(useremail);      

    if(emailFlag){
      return true;
    }

    else{
       Alert.alert('', locale.t('enterValidEmail'), [{text: locale.t('ok')}]);
      return false;  
    }
  }
}

export function passwordValidation(pass){
  let noSpace = false
  let inValid = /\s/;
  noSpace = inValid.test(pass);

  if(pass == ""){
    Alert.alert('', locale.t('enterPassword'), [{text: locale.t('ok')}]);      
    return false;
  }
  if(pass.length < 5){
    Alert.alert('', locale.t('passwordsMustHaveAtleastCharacters'), [{text: locale.t('ok')}]);
    return false;
  }
  if(noSpace) {
    Alert.alert('', locale.t('passwordMustNotContainSpaces'), [{text: locale.t('ok')}]);
    return false;
  }
  else
    return true;

}
