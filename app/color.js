//colors

//export const black = 'rgba(255, 255, 255, 1.0)'
export const black = '#000000'
export const white = '#ffffff'
export const blue = '#0000ff'
export const iceblue = '#f0f8ff'
export const burlywood = '#deb887'
export const darkgrey = '#a9a9a9'
export const apptheme = '#5DBCBE'
export const coralred = '#EB7F74'
export const lightgray = 'rgba(159, 159, 159, 1.0)'
export const transparent = 'rgba(52, 52, 52, 0.2)'
export const red = '#FF0000'