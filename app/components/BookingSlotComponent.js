import React, { Component } from "react";
import { apptheme, white, darkgrey, coralred } from "../color";
import { connect } from "react-redux";
import moment from "moment";
import Dimensions from "Dimensions";
import styles from "../assets/styles/BookingSlotComponentStyles";

import {
  AppRegistry,
  StyleSheet,
  View,
  Text,
  ListView,
  TouchableOpacity,
  PixelRatio
} from "react-native";

class BookingSlotComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      itemdata: props.datasource,
      selectedIndex: -1 //by default no time slot is selected
    };

    this.onSelectItem = this.onSelectItem.bind(this);
    this._renderRow = this._renderRow.bind(this);
    this.setSelectedIndexIfAvailable = this.setSelectedIndexIfAvailable.bind(this);

    this.width = Math.floor(Dimensions.get("window").width);
    //var ratio = PixelRatio.get();
    switch (Number(this.width)) {
      case 320: //ios
        this.width = Dimensions.get("window").width - 35;
        break;
      case 360: //Android
        this.width = Dimensions.get("window").width - 10;
        break;
      case 375: //ios
        this.width = Dimensions.get("window").width - 20;
        break;
      case 384: //Android
        console.log("size: it gets here");
        this.width = Dimensions.get("window").width - 30;
        break;
      case 414: //ios
      case 411: //Andorid
        console.log("size: it gets here 411");
        this.width = Dimensions.get("window").width - 60;
        break;
      case 768: //ipad
        this.width = Dimensions.get("window").width - 125;
      default:
        break;
    }
  }

  componentDidMount() {
    //show already selected timeslot for first page
    if (
      Object.keys(this.props.selectedSlots).length > 0 &&
      this.props.datasource.length > 0 &&
      this.props.selectedSlots[this.props.datasource[0].serviceId]
    ) {
      this.setState({
        selectedIndex: this.props.selectedSlots[
          this.props.datasource[0].serviceId
        ].id
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      itemdata: nextProps.datasource
    });

    //show already selected timeslot
    if (
      Object.keys(nextProps.selectedSlots).length > 0 &&
      nextProps.datasource.length > 0
    ) {
      let tempSlot = nextProps.selectedSlots[nextProps.datasource[0].serviceId];
      if (tempSlot) {
        this.setState({
          selectedIndex: tempSlot.id
        });
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ListView
          contentContainerStyle={styles.listStyle}
          dataSource={ds.cloneWithRowsAndSections(this.state.itemdata)}
          renderSectionHeader={this._section_Header}
          renderRow={this._renderRow}
          enableEmptySections={true}
          stickySectionHeadersEnabled={false}
        />
      </View>
    );
  }

  _section_Header(data, sectionId) {
    return (
      <View style={styles.employeeSection}>
        <View style={styles.employeeSectionBorder} />

        <Text style={styles.employeeNameText}>
          {data.empolyeeData.EmployeefirstName +
            " " +
            data.empolyeeData.EmployeelastName}
        </Text>

        <View style={styles.employeeSectionBorder} />
      </View>
    );
  }

  setSelectedIndexIfAvailable = (id, isAvailable) => {
    if (isAvailable) {
      this.setState({
        selectedIndex: id
      });
    }
  };

  onSelectItem = (timeslot, section) => {
    if (timeslot.id !== this.state.selectedIndex) {
      selectedtime = timeslot.slot.replace(/\s/g, "");
      bookingObject = {
        name:
          this.state.itemdata[section].empolyeeData.EmployeefirstName +
          " " +
          this.state.itemdata[section].empolyeeData.EmployeelastName,
        employee: this.state.itemdata[section].empolyeeData,
        slot: selectedtime.toLowerCase(),
        id: timeslot.id,
        section: section
      };

      //update parent with selected time
      this.props.serviceSelected(
        bookingObject,
        this.setSelectedIndexIfAvailable
      );
    }
  };

  _renderRow(data, sectionID, rowID) {
    return (
      <View style={{ width: Dimensions.get("window").width }}>
        {
          <View style={{ width: this.width, alignSelf: "center" }}>
            <RadioButtonTimeslots
              data={data.EmployeeShifts}
              selectedIndex={this.state.selectedIndex}
              sectionIndex={sectionID}
              onSelectItem={this.onSelectItem}
            />
          </View>
        }
      </View>
    );
  }
}

class RadioButtonTimeslots extends Component {
  constructor(props) {
    super(props);

    this.circleStyleUnselected = {
      view: "circleStyleUnselected"
    };
    this.circleStyleSelected = {
      view: "circleStyleSelected"
    };

    this.state = {
      unselectedCircleStyle: this.circleStyleUnselected,
      selectedCircleStyle: this.circleStyleSelected
    };
  }

  render() {
    return Array.isArray(this.props.data) ? (
      <View style={styles.listStyle}>
        {this.props.data.map(
          (item, i) =>
            this.props.selectedIndex === item.id ? (
              <TouchableOpacity
                key={i}
                style={styles[this.state.selectedCircleStyle.view]}
                onPress={() =>
                  this.props.onSelectItem(item, this.props.sectionIndex)}
              >
                <Text style={styles.itemSlotText}>{item.slot.substring(0,5)}</Text>
                <Text style={styles.itemSlotText}>{item.slot.substring(5)}</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                key={i}
                style={styles[this.state.unselectedCircleStyle.view]}
                onPress={() =>
                  this.props.onSelectItem(item, this.props.sectionIndex)}
              >
                <Text style={styles.itemSlotText}>{item.slot.substring(0,5)}</Text>
                <Text style={styles.itemSlotText}>{item.slot.substring(5)}</Text>
              </TouchableOpacity>
            )
        )}
      </View>
    ) : (
      <View />
    );
  }
}

const ds = new ListView.DataSource({
  rowHasChanged: (r1, r2) => r1 !== r2,
  sectionHeaderHasChanged: (s1, s2) => s1 !== s2
});

function mapStateToProps(state) {
  return {
    selectedSlots: state.selectedSlots
  };
}

 export default connect(mapStateToProps)(BookingSlotComponent);
