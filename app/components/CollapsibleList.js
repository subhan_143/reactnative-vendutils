import React, { Component } from "react";
import { connect } from "react-redux";
import Collapsible from "react-native-collapsible";
import InfoDialog from "./InfoDialog";
import CardHeader from "./CardHeader";
import CollapsibleServiceView from "./CollapsibleServiceView";
import CollapsibleOfferView from "./CollapsibleOfferView";

import { View, StyleSheet, TouchableOpacity } from "react-native";

class CollapsibleList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      collapsed: props.collapseAllTabs,
      isVisible: false,
      infolist: [],
      serviceType: props.type
    };
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.collapseAllTabs !== this.props.collapseAllTabs) {
      this.setState({ collapsed: nextProps.collapseAllTabs });
    }
    if (nextProps.type !== this.props.type) {
      this.setState({ serviceType: nextProps.type });
    }
    //collapse all open services when tab changes
    if (nextProps.tabIndex !== this.props.tabIndex &&
      nextProps.tabIndex !== 1 &&
      !this.props.isOffer) {
      this.setState({ collapsed: true });
    }

  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.collapsed !== this.state.collapsed ||
          nextState.collapseAllTabs !== this.state.collapseAllTabs ||
          nextState.isVisible !== this.state.isVisible ||
          nextState.infolist !== this.state.infolist ||
          nextState.serviceType !== this.state.serviceType ||
          nextProps.datasource !== this.props.datasource;
  }
  

  //close info dialog box
  closeInfo(value) {
    this.setState({ isVisible: false });
  }

  //expand/collapse list items
  _toggleExpanded = () => {
    this.setState({ collapsed: !this.state.collapsed });
  }

  showInfoBox = data => {
    this.setState({ isVisible: true, infolist: data });
  }

  _renderHeader(data, index) {
    return (
      <TouchableOpacity onPress={this._toggleExpanded}>
        <CardHeader
          data={data}
          toggleSelected={false}
          activeSection={this.state.collapsed}
        />
      </TouchableOpacity>
    );
  }

  _renderRow(data) {
    //set checked already selected services
    if (this.props.isOffer) {
      return (
        <CollapsibleOfferView
          data={data}
          isCollapsed={this.state.collapsed}
          oninfo={this.showInfoBox}
          serviceType={this.state.serviceType}
        />
      );
    }
    return (
      <CollapsibleServiceView
        data={data.content}
        isCollapsed={this.state.collapsed}
        oninfo={this.showInfoBox}
        serviceType={this.state.serviceType}
      />
    );
  }

  render() {
    return (
      <View>
        {this.props.isOffer ? (
          <View />
        ) : (
          this._renderHeader(this.props.datasource, this.props.index)
        )}
        {this._renderRow(this.props.datasource)}

        <InfoDialog
          infolist={this.state.infolist}
          visible={this.state.isVisible}
          callback={this.closeInfo.bind(this)}
        />
      </View>
    );
  }
}


function mapStateToProps(state) {
  return {
    tabIndex: state.tabIndex
  };
}

function mapDispatchToProps(dispatch) {
  return {
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsibleList);
