import React, { Component } from "react";
import { connect } from "react-redux";
import {
  selected_bookingslots,
  selected_slots
} from "../actions/bookingslotActions";
import { selected_offers } from "../actions/offerActions";
import Collapsible from "react-native-collapsible";
import CardRow from "./CardRow";
import { locale } from "../containers/AppContainer";

import { View, StyleSheet, Alert } from "react-native";


class CollapsibleOfferView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedOffers: props.selectedOffers,
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedOffers: nextProps.selectedOffers,
    });
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    return nextState.selectedOffers !== this.state.selectedOffers ||
          nextProps.isCollapsed !== this.props.isCollapsed ||
          nextProps.data !== this.props.data;
  }


  findOfferItem(data) {
    if (this.state.selectedOffers.length > 0 &&
      this.state.selectedOffers.find(offer => offer.id === data.id)) {
      return true;
    }
    return false;
  }

  addRemoveOffer = (toggleSelected, offer, callback) => {
    if(this.props.selectedServices.length > 0 && this.props.selectedServices[0].isHomeBased) {
      Alert.alert(
        '',
        locale.t('bookSalonAndServiceSeparately'),
        [{text: locale.t('ok')}]
      );

      return;
    }
    //update checkbox selection
    callback();

    if (!toggleSelected) {
      this.state.selectedOffers.push(offer);
    } else {
      //remove item from array
      this.state.selectedOffers = this.state.selectedOffers.filter(
        element => element.id !== offer.id
      );

      //if this removed offer exist in selected timeslots remove it
      if (Object.keys(this.props.selectedBookings).length > 0 &&
        Object.keys(this.props.selectedBookings.data).length > 0) {
        var timeslots = this.props.selectedSlots;
        var deletedOffer = this.props.selectedBookings.data[`_${offer.id}_`];

        if (deletedOffer) {
          for (key in deletedOffer.content) {
            delete timeslots[`_${offer.id}${key}`];
          }
        }

        this.props.saveSelectedSlots(timeslots); //update selected timeslots

        var bookings = this.props.selectedBookings.data;
        if (bookings) delete bookings[`_${offer.id}_`];

        this.props.updateBookings({ data: bookings }); //update booking object
      }
    }

    //update store, local state will be updated automatically
    this.props.saveSelectedOffers(this.state.selectedOffers);
  }

  render() {
    return(
      <View>
        <Collapsible collapsed={this.props.isCollapsed} align="center">
          <CardRow
            item={this.props.data}
            onSelectCard={this.addRemoveOffer}
            oninfo={this.props.oninfo}
            toggleSelected={this.findOfferItem(this.props.data)}
            isOffer={true}
          />
        </Collapsible>
      </View>
    )
  }

}

function mapStateToProps(state) {
  return {
    //need this to remove bookingslot if user uncheck the card, after coming from summary page
    selectedBookings: state.selectedBookings,
    selectedServices: state.selectedServices,
    selectedOffers: state.selectedOffers,
    selectedSlots: state.selectedSlots
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateBookings: dataSet => dispatch(selected_bookingslots(dataSet)),
    saveSelectedOffers: offerlist => dispatch(selected_offers(offerlist)),
    saveSelectedSlots: slots => dispatch(selected_slots(slots))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsibleOfferView);
