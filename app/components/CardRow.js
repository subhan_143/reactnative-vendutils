import React, { Component } from 'react';
import RoundCheckbox from 'rn-round-checkbox';
import {getUser} from '../models/userModel';
import CardView from 'react-native-cardview'
import {convertToHourMinute} from "../utils/helpers";
import styles from '../assets/styles/AccordionListStyles';
import {locale} from '../containers/AppContainer';
import Collapsible from 'react-native-collapsible';
import { apptheme } from '../color';

import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback
} from "react-native";

export default class CardRow extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      toggleSelected: props.toggleSelected
    };
    this.users = getUser();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      toggleSelected: nextProps.toggleSelected
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.toggleSelected !== this.state.toggleSelected ||
          nextProps.item !== this.props.item;
  }
  
  setSelected = () => {
    this.setState({
      toggleSelected: !this.state.toggleSelected
    });
  }

  toggleSelected = () => {
    this.props.onSelectCard(this.state.toggleSelected, this.props.item, this.setSelected);
  };

  service_detail(data) {
    infolist = [];
    contentList = [];
    if(this.props.isOffer) {
      data.content.map((item, i ) => {
        contentList.push({
          serviceName: item.name,
          serviceDescription: item.description
                              ? item.description
                              : locale.t("noDescriptionAvailable")
        });
      })

      infolist.push({
        heading: data.name,
        services: contentList,
        text: data.description
          ? data.description
          : locale.t("noDescriptionAvailable")
      });
    }

    else {
      infolist.push({
        heading: data.name,
        text: data.description
          ? data.description
          : locale.t("noDescriptionAvailable")
      });
    }
    this.props.oninfo(infolist);
  }

  render() {
    return(
      <View >
            <CardView
              style={styles.cardview}
              cardElevation={1}
              cardMaxElevation={1}
              cornerRadius={12}
            >
            <TouchableWithoutFeedback onPress = {this.toggleSelected}>
              <View>
              <View style={[styles.cardrow, { justifyContent: "flex-end" }]}>
                {this.props.item.isPrepayment ? (
                  <Text style={styles.viewMoreText}>Down payment</Text>
                ) : (
                  <View />
                )}
              </View>

              <View style={[styles.cardrow, styles.titleCheckboxSection]}>
                <View style={styles.titleSection}>
                  <Text style={styles.titleText}>
                    {this.props.item.name.toUpperCase()}
                  </Text>
                </View>


                  <View style={styles.checkBoxSection}>
                    <RoundCheckbox
                      size={22}
                      checked={this.state.toggleSelected}
                      onValueChange={newValue => {
                        this.toggleSelected();
                      }}
                      backgroundColor={apptheme}
                    />
                  </View> 
              </View>
              
              {!this.props.isOffer ?
                
                null
                : 
                <View style={[styles.cardrow]}>
                  {this.props.item.description ? (
                    <Text style={styles.durationText}>
                      {this.props.item.description}
                    </Text>
                  ) : (
                    <Text style={styles.durationText}>
                      {locale.t("noDescriptionAvailable")}
                    </Text>
                  )}
                </View>
              }
              
              <View style={[ styles.cardrow, styles.viewMoreSection ]}>
                  <TouchableOpacity onPress={()=> {this.service_detail(this.props.item)}}>
                    <Text style={styles.viewMoreText}>{locale.t('viewMore')}</Text>
                  </TouchableOpacity>
                {
                  this.props.item.price ?
                  <Text style={styles.currencyNameText}>{this.users.userCountry.currency_code} {this.props.item.price} </Text> :
                  <View />
                }
              </View>
                </View>
              </TouchableWithoutFeedback>
            </CardView>
      </View>

    );
  }
}
