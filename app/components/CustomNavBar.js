import React, { Component, PropTypes, DefaultProps } from "react";
import { View, Image, TouchableOpacity, Text, TextInput, Keyboard, Platform } from "react-native";
import { Actions } from "react-native-router-flux";
import { apptheme, white } from "../color";
import Icon from "react-native-vector-icons/Ionicons";
import { locale } from "../containers/AppContainer";
import Dimensions from "Dimensions";

var debounce = require('lodash.debounce');

class CustomNavBar extends Component {
  constructor(props) {
    super(props);
    this.openDrawer = this.openDrawer.bind(this);
    this.openMap = this.openMap.bind(this);
    this.goBack = this.goBack.bind(this);
    this.state = {
      is_searching: false
    };

    this.imageCollection = {
      hair: require("../img/hair_navbar.png"),
      homeservice: require("../img/home_navbar.png"),
      "makeup&lashes": require("../img/makeup_navbar.png"),
      nails: require("../img/nails_navbar.png"),
      spa: require("../img/spa_navbar.png"),
      grooming: require("../img/grooming_navbar.png")
    };
  }

  goBack() {
    if (this.props.onBackPress) {
      this.props.onBackPress();
    } else {
      Actions.pop();
    }
  }

  openDrawer() {
    this.props.toggleMenu();
    this.setState({is_searching: false});
  }

  openMap() {
    Actions.map();
  }

  searchSalon = (input) => {
    this.props.filterSalons(input.trim());
  }

  render() {
    return (
      <View style={styles.tabView}>
        <View style={styles.mainView}>
          <View style={styles.navBar}>
            <TouchableOpacity
              style={styles.leftBarButton}
              onPress={this.goBack} >
              <Icon
                name="ios-arrow-back"
                size={24}
                color="#fff"
                style={{
                  transform: [{ scaleX: locale.locale === "arb" ? -1 : 1 }]
                }}
              />
            </TouchableOpacity>
            { !this.state.is_searching ?
            <TouchableOpacity
              style={styles.leftBarButtonSort}
              onPress={() => {
                this.props.sort();
              }}
            >
              <Text style={{ color: white, fontSize: 18}}>
                {locale.t("sort")}
              </Text>
            </TouchableOpacity> : <View />
            }
          </View>

          {/*<Image style={ styles.navBar } source={require(`../img/${this.props.imagename}.png`)} />*/}
          { !this.state.is_searching ?
          <Image
            style={{marginTop: Math.floor(Dimensions.get("window").height) === 812 ? 45 : 25}}
            resizeMode={"contain"}
            source={this.imageCollection[this.props.imagename]}
          /> : <View />
          }
          <View style={[styles.navBar, {alignItems: "flex-end"}]}>
            <View style={{ flexDirection: "row", marginRight: 5}}>
              <TouchableOpacity
                style={styles.rightBarButton}
                onPress={() => { this.setState({is_searching: !this.state.is_searching}) }} >
              <Icon
                name="ios-search"
                size={24}
                color="#fff"
                style={{
                  transform: [{ scaleX: locale.locale === "arb" ? -1 : 1 }]
                }}
              />
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.rightBarButton}
                onPress={this.openDrawer} >
                <Icon 
                  name="ios-menu" 
                  size={24} 
                  color="#fff" />
              </TouchableOpacity>
            </View>

            { !this.state.is_searching && this.props.enableMap ?
            <TouchableOpacity
              style={styles.rightBarButtonMap}
              onPress={() => {
                // this.openMap();
                this.props.map();
              }}
            >
              <Text style={{ color: white, fontSize: 18}}>
                {locale.t("map")}
              </Text>
            </TouchableOpacity> : <View />
            }
          </View>
        </View>
        {
          this.state.is_searching ?
          <View style={styles.searchSection}>
            <Icon
              name="ios-search"
              size={20}
              color="#a6a6a6"
              style={{
                transform: [{ scaleX: locale.locale === "arb" ? -1 : 1 }],
                paddingLeft: 10,
                paddingRight:10,
                paddingBottom: 2,
                marginLeft: "auto"
              }}
            />
            <TextInput
              style = {{
                fontSize: 14, 
                paddingLeft: 13, 
                borderRadius: 5, 
                textAlign: locale.locale === "arb" ? "right" : "left",
                backgroundColor: "white", 
                height: Platform.OS === "ios" ? 28 : 40, 
                width: Dimensions.get("window").width-20, 
                marginLeft: "auto"
              }}
              placeholder={locale.t("searchByBusiness")}
              returnKeyType={"search"}
              underlineColorAndroid={"transparent"}
              autoFocus={true}
              onChangeText={debounce(this.searchSalon, 500)}
              onSubmitEditing={() => { 
                this.setState({is_searching: false});
                Keyboard.dismiss();
              }}
            /> 
          </View>
          : <View />
        }
      </View>
    );
  }
}

CustomNavBar.propTypes = {
  imagename: PropTypes.string
};

CustomNavBar.defaultProps = {
  imagename: ""
};

const styles = {
  tabView: {
    backgroundColor: apptheme,
    height: Math.floor(Dimensions.get("window").height) === 812 ? 120 : 100,
  },
  mainView: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  navBar: {
    flexDirection: "column",
    marginTop: Math.floor(Dimensions.get("window").height) === 812 ? 49 : 29
  },
  leftBarButton: {
    width: 85,
    alignItems: "flex-end",
    flexDirection: "row",
    paddingHorizontal: 10,
    marginBottom: 12
  },
  leftBarButtonSort: {
    width: 85,
    backgroundColor: "transparent",
    alignItems: "flex-end",
    flexDirection: "row",
    paddingLeft: 23,
    marginBottom: 12
  },
  rightBarButton: {
    alignItems: "flex-end",
    paddingHorizontal: 10,
    flexDirection: "row",
    marginBottom: 12
  },
  rightBarButtonMap: {
    alignItems: "flex-end",
    backgroundColor: "transparent",
    paddingRight: 20,
    flexDirection: "row",
    marginBottom: 12
  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  }
};

export default CustomNavBar;
