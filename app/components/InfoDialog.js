import React, { Component } from "react";
import { connect } from "react-redux";
import * as Progress from "react-native-progress";
import Dimensions from "Dimensions";
import Modal from "react-native-modal";
import { apptheme, white } from "../color";
import { locale } from "../containers/AppContainer";

import {
  View,
  Text,
  StyleSheet,
  ListView,
  Image,
  TouchableOpacity,
  Alert,
  ScrollView
} from "react-native";

class InfoDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: props.visible
    };

  }

  componentWillReceiveProps(nextProps) {
    this.setState({ visible: nextProps.visible });
  }

  getServices(services) {
    return services.map((item, i) => {
      return (
          <View style={styles.text} key={i}>
          <Text style={[styles.body,{fontFamily: 'Quicksand-Bold'}]}>{item.serviceName}</Text>
          <Text style={styles.body}>{item.serviceDescription}</Text>
          </View>
        )
    })
  }

  render() {
    return (
      <View>
        {
          <View style={styles.container}>
              <Modal isVisible={this.state.visible} style={{paddingVertical: 20}}>
                <View style={styles.bodyContainer}>
                  <ScrollView>
                      {this.props.infolist.map((data, i) => {
                        return (
                          <View style={styles.text} key={i}>
                            {
                              !data.services ? 
                              <View style={styles.text}>
                              <Text style={styles.heading}>{data.heading}</Text>

                              <Text style={styles.body}>{data.text}</Text>
                              </View>
                              :

                              <View/>
                            }
                            
                            {data.time != undefined ? (
                              <Text style={styles.body}>
                                {" "}
                                {locale.t("appointmentTime")} {data.time}
                              </Text>
                            ) : (
                              <View/>
                            )}
                            {data.employee != undefined ? (
                              <Text style={styles.body}>
                                {" "}
                                {locale.t("serviceProvider")} {data.employee}
                              </Text>
                            ) : (
                              <View/>
                            )}
                            {
                              data.services ? 
                                this.getServices(data.services)
                              :
                                <Text/>

                            }
                          </View>
                        );
                      })}
                  </ScrollView>
                 

                  <View
                    style={{
                      width: "100%",
                      borderBottomWidth: 1.5,
                      marginTop: 10,
                      borderColor: "#E7E7E7"
                    }}
                  />

                  <TouchableOpacity
                    onPress={() => {
                      this.setState({ visible: false });
                      this.props.callback(false);
                    }}
                    style={{ paddingHorizontal: 100 }}
                  >
                    <Text style={styles.buttonstyle}>{locale.t("ok")}</Text>
                  </TouchableOpacity>
                </View>
              </Modal>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bodyContainer: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: white,
    borderRadius: 10
  },
  text: {
    alignItems: "center",
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 15
  },
  heading: {
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    marginBottom: 10
  },
  body: {
    fontSize: 14,
    marginBottom: 10
  },
  buttonstyle: {
    fontWeight: "bold",
    fontSize: 16,
    color: "#007AFF",
    marginTop: 10,
    marginBottom: 10
  }
});

export default InfoDialog;
