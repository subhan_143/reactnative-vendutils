import React, { Component } from "react";
import { connect } from "react-redux";
import * as Progress from "react-native-progress";
import { StyleSheet, View } from "react-native";
import Dimensions from "Dimensions";
import PropTypes from 'prop-types';


class Loader extends Component {

  static propTypes = {
    size: PropTypes.number
  }
  static defaultProps = {
    size: 40
  }

  constructor(props) {
    super(props);

    this.state = {
      show: props.show
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errorMessage.message !== "") {
      this.setState({ show: false });
    }
  }

  render() {
    return (
      <View>
        {this.state.show ? (
          <View>
            <Progress.Circle
              size={this.props.size? this.props.size: defaultProps.size}
              thickness={10}
              indeterminate={true}
              color={this.props.loadercolor? this.props.loadercolor: "white" }
            />
          </View>
        ) : (
          <View />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    errorMessage: state.errorMessage
  };
};

export default connect(mapStateToProps)(Loader);
