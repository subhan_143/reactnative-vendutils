import React, { Component } from 'react';
import { TabViewAnimated, TabBar, SceneMap, TabViewPagerPan, TabViewPagerScroll } from 'react-native-tab-view';
import { apptheme } from '../color';
import StaffComponent from '../components/StaffComponent';
import InfoComponent from '../components/InfoComponent';
import ServiceComponent from './ServiceComponent';
import OfferComponent from './OfferComponent';
import {locale} from '../containers/AppContainer';

import {
  StyleSheet,
  Text,
  View,
  Platform,
  Dimensions,
} from "react-native";

const insta = require("../img/instagram.png");
const snapChat = require("../img/snapchat.png");

let screenWidth = Math.floor(Dimensions.get("window").width);

class TabsViewComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      index: props.tab_id? (props.tab_id - 1): 1,
      routes: [
        { key: "1", title: locale.t("info") },
        { key: "2", title: locale.t("services") },
        { key: "3", title: locale.t("offers") },
        { key: "4", title: locale.t("staff") }
      ]
    };

    this._renderScene = SceneMap({
      "1": this.InfoPage,
      "2": this.ServicePage,
      "3": this.OfferPage,
      "4": this.StaffPage
    });

  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.index !== this.state.index;
  }

  InfoPage = () => <InfoComponent ref={component => this._infoComp = component} salondata = {this.props} />
  StaffPage = () => <StaffComponent ref={component => this._staffComp = component} salonId = {this.props.salon_id} />
  ServicePage = () => <ServiceComponent ref={component => this._servComp = component} salonId = {this.props.salon_id} saloonName = {this.props.salon_name} 
    onFilterChange = {(type)=> {this.props.onFilterChange(type)}} onSearchService = {(isSearching) => {this.props.onSearchService(isSearching)}} {...this.props} />
  OfferPage = () => <OfferComponent ref={component => this._offerComp = component} salonId = {this.props.salon_id} saloonName = {this.props.salon_name} />


  _renderHeader = props => {
    return (
      <TabBar
        {...props}
        style={styles.tabBar}
        labelStyle={{
          color: "#565656",
          fontSize: screenWidth > 360 ? 11 : screenWidth < 360 ? 9: 10,
          fontFamily: "Quicksand-Bold"
        }}
        indicatorStyle={{ backgroundColor: apptheme }}
        onTabPress={el => {
          console.log(el);
        }}
      />
    );
  };

  
  _renderPager = props => {
    return Platform.OS === "ios" ? (
      <TabViewPagerScroll swipeEnabled={false} {...props} />
    ) : (
      <TabViewPagerPan swipeEnabled={false} {...props} />
    );
  };

  render() {
    return (
      <TabViewAnimated
        style={styles.container}
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        renderPager={this._renderPager}
        onIndexChange={index => {
          this.props.onTabChange(index);
          this.setState({ index });
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  tabBar: {
    backgroundColor: 'transparent',
  }
});

export default TabsViewComponent;
