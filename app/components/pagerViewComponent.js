import React, { Component } from "react";
var ViewPager = require("react-native-viewpager");
import Image from "react-native-image-progress";
import * as Progress from "react-native-progress";

import { AppRegistry, StyleSheet, View, Dimensions, Text } from "react-native";
import { apptheme } from "../color";
import { locale } from "../containers/AppContainer";


class PagerViewComponent extends Component {
  constructor(props) {
    super(props);
    this.imgArray = [];
    this.isEmpty = false;
    this.isPortfolio = false;

    this.customStyles = StyleSheet.create({
        page: {
          width: Dimensions.get("window").width,
          height: Dimensions.get("window").height/2,
        }
    });
    
    
    if(props.salonGallery && props.gallery != null){
      this.setImagesList(props);
    }
    else{
      this.isEmpty = true;
    }
    
    this.state = {
      dataSource: ds.cloneWithPages(this.imgArray),
      imageArrayLength: this.imgArray.length
    };
  }


  setImagesList(data){
    this.imgArray = []
    
    data.gallery.map((salon)=>{
      this.imgArray.push(salon.salonImages);
    })
  }

  componentWillReceiveProps(nextProps) {
      if (nextProps.staffCheck) {
        this.isPortfolio = true;
        this.isEmpty = false
        this.setState({
          dataSource: ds.cloneWithPages(nextProps.imageData),
          imageArrayLength: nextProps.imageData.length
        });
      }
      else if (nextProps.salonGallery && nextProps.gallery != null){ 
        this.setImagesList(nextProps);
        this.setState({
          dataSource: ds.cloneWithPages(this.imgArray),
          imageArrayLength: this.imgArray.length
        });
      }
      else 
        this.isEmpty = true;
      
  }
  

  render() {
    return !this.isEmpty ? (
      <View style={this.isPortfolio ? this.customStyles.page : styles.page}>
        <ViewPager
          style={styles.page}
          dataSource={this.state.dataSource}
          renderPage={this._renderPage}
          isLoop={this.state.imageArrayLength > 1}
          autoPlay={true}
        />
      </View>
    ) : (
      <View style={styles.notAvailableSection}>
        <Text style={styles.notAvailableText}>{locale.t("noMessageAvailable")}</Text>
      </View>
    );
  }

  _renderPage(data, pageID) {
    return (
      <Image
        source={{ uri: data }}
        resizeMode={"stretch"}
        indicator={Progress.Circle}
        indicatorProps={{
          size: 40,
          color: "#5DBCBE",
          unfilledColor: "rgba(200, 200, 200, 0.2)"
        }}
        style={styles.image}
      />
    );
  }
}

const ds = new ViewPager.DataSource({
  pageHasChanged: (p1, p2) => p1 !== p2
});

var styles = StyleSheet.create({
  page: {
    // width: Dimensions.get("window").width,
    height: Dimensions.get("window").height/3,
  },
  image:{
    flex: 1,
    width: null,
    height: null,
  },
  notAvailableSection: {
    height: Dimensions.get("window").height/3,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  notAvailableText: {
    textAlign: "center",
    fontSize: 20
  }
});

export default PagerViewComponent;
