import React, { Component } from "react";
import { connect } from "react-redux";
import {
  selected_bookingslots,
  selected_slots
} from "../actions/bookingslotActions";
import { selected_services } from "../actions/serviceActions";
import Collapsible from "react-native-collapsible";
import CardRow from "./CardRow";
import { locale } from "../containers/AppContainer";

import { View, StyleSheet, Alert } from "react-native";


class CollapsibleServiceView extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedServices: props.selectedServices
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedServices: nextProps.selectedServices,
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.selectedServices !== this.state.selectedServices ||
          nextProps.isCollapsed !== this.props.isCollapsed ||
          nextProps.data !== this.props.data;
  }
  

  findItem(data) {
    if (this.state.selectedServices.length > 0 &&
      this.state.selectedServices.find(service => service.id === data.id)) {
      return true;
    }
    return false;
  }

  addRemoveService = (toggleSelected, service, callback) => {
    //do not allow to select home based service if offer is already selected
    if((this.props.selectedOffers.length > 0 && this.props.serviceType === "home")) {
      Alert.alert(
        '',
        locale.t('bookSalonAndServiceSeparately'),
        [{text: locale.t('ok')}]
      );

      return;
    }
    //update checkbox selection
    callback();
    
    if (!toggleSelected) {
      this.state.selectedServices = this.state.selectedServices.slice(0);
      this.state.selectedServices.push(service);
    } else {
      //remove item from array
      this.state.selectedServices = this.state.selectedServices.filter(
        element => element.id !== service.id
      );

      //if this removed service exist in selected timeslots remove it
      if (
        Object.keys(this.props.selectedBookings).length > 0 &&
        Object.keys(this.props.selectedBookings.data).length > 0
      ) {
        var timeslots = this.props.selectedSlots;
        delete timeslots[`_${service.id}`];
        this.props.saveSelectedSlots(timeslots); //update selected timeslots

        var bookings = this.props.selectedBookings.data;
        delete bookings[`_${service.id}`];

        this.props.updateBookings({ data: bookings }); //update booking object
      }
    }

    //update store, local state will be updated automatically
    this.props.saveSelectedServices(this.state.selectedServices);
  }

  render() {
    return(
      <View>
      {this.props.data.map((item, i) => {
        return (
          <Collapsible key={i} collapsed={this.props.isCollapsed} align="center">
            <CardRow
              item={item}
              onSelectCard={this.addRemoveService}
              oninfo={this.props.oninfo}
              toggleSelected={this.findItem(item)}
              isOffer={false}
            />
          </Collapsible>
        );
      })}
    </View>  
    )
  }
}

function mapStateToProps(state) {
  return {
    //need this to remove bookingslot if user uncheck the card, after coming from summary page
    selectedBookings: state.selectedBookings,
    selectedServices: state.selectedServices,
    selectedOffers: state.selectedOffers,
    selectedSlots: state.selectedSlots
  };
}

function mapDispatchToProps(dispatch) {
  return {
    updateBookings: dataSet => dispatch(selected_bookingslots(dataSet)),
    saveSelectedServices: servicelist => dispatch(selected_services(servicelist)),
    saveSelectedSlots: slots => dispatch(selected_slots(slots))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CollapsibleServiceView);
