import React, { Component } from "react";
var ViewPager = require("react-native-viewpager");
import BookingSlotComponent from "./BookingSlotComponent";
import { locale } from "../containers/AppContainer";

import {
  AppRegistry,
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image
} from "react-native";

var dataSource = new ViewPager.DataSource({
  pageHasChanged: (p1, p2) => p1 !== p2
});

class TimingSlotPager extends Component {
  constructor(props) {
    super(props);

    pages = Object.values(props.datasource).map((value, index) => {
      return value;
    });

    this.state = {
      dataSource: dataSource.cloneWithPages(pages)
    };
  }

  componentWillReceiveProps(nextProps) {
    pages = Object.values(this.props.datasource).map((value, index) => {
      return value;
    });

    this.setState({
      dataSource: dataSource.cloneWithPages(pages)
    });
  }

  componentDidUpdate(prevProps, prevState) {
    this.refs.pager.goToPage(this.props.currentPage);
  }

  render() {
    return (
      <ViewPager
        ref="pager"
        style={{ flex: 1 }}
        dataSource={this.state.dataSource}
        renderPage={this._renderPage.bind(this)}
        renderPageIndicator={false}
        locked={true}
        /*onChangePage={(index) => { this.props.onPageChange(index) }}*/
      />
    );
  }

  _renderPage(dataArray, pageID) {
    return dataArray.length > 0 ? (
      <BookingSlotComponent
        datasource={dataArray}
        pageIndex={this.props.currentPage}
        serviceSelected={this.props.serviceSelected}
      />
    ) : (
      <Text style={[styles.page, { color: "rgb(233, 12, 71)" }]}>
        {" "}
        {locale.t("noTimeSlotAvailable")}{" "}
      </Text>
    );
  }
}

const styles = StyleSheet.create({
  page: {
    width: Dimensions.get("window").width,
    textAlign: locale === "arb" ? "right" : "left",
    paddingLeft: 7,
    paddingRight: 7
  }
});

export default TimingSlotPager;
