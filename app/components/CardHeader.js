import React, { Component } from 'react';
import styles from '../assets/styles/AccordionListStyles';
import Icon from 'react-native-vector-icons/Ionicons';
import {locale} from '../containers/AppContainer';
import { apptheme } from '../color';

import {
  View,
  Text,
  StyleSheet,
  Alert
} from "react-native";

export default class CardHeader extends Component {
  constructor(props) {
    super(props);

  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps.activeSection !== this.props.activeSection ||
          nextProps.data !== this.props.data;
  }
  
  render() {
    return (
      <View style={{ flexDirection: "column" }}>
        <View style={[styles.header]}>
          <Text style={styles.headerText}>
            {this.props.data.name.toUpperCase()}
          </Text>
          {
          this.props.data.isPrepayment &&
          this.props.data.content.length > 0 ? (
            <Text style={styles.headerText}>(Prepayment)</Text>
          ) : (
            <View />
          )}
          {!this.props.activeSection ? (
            <Icon
              style={{ marginRight: "auto" }}
              name="ios-arrow-down"
              size={16}
              color={apptheme}
            />
          ) : (
            <Icon
              style={{
                marginRight: "auto",
                transform: [{ scaleX: locale.locale === "arb" ? -1 : 1 }]
              }}
              name="ios-arrow-forward"
              size={16}
              color={apptheme}
            />
          )}
        </View>
        {!this.props.activeSection ? (
          <View />
        ) : (
          <View
            style={{
              // borderBottomWidth: 1.5,
              // borderColor: "#E7E7E7"
            }}
          />
        )}
      </View>
    );
  }
}

