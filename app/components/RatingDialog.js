import React, { Component } from "react";
import { connect } from "react-redux";
import Modal from "react-native-modal";
import { apptheme, white, coralred, transparent } from "../color";
import { locale } from "../containers/AppContainer";
import StarRating from 'react-native-star-rating';

import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  TextInput
} from "react-native";

class RatingDialog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: props.visible,
      starCount: 0,       
    };

    this.userComments = ''
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating
    });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ visible: nextProps.visible });
  }

  render() {
    return (
      <View>
        {
          <View style={styles.container}>
            <ScrollView style={styles.scrollView}>
              <Modal isVisible={this.state.visible}>
                
                <View style={styles.bodyContainer}>
                  
                  <View style={{width: "100%",flexDirection: 'column'}}>
                    <Text style={{textAlign: 'center', marginTop: 10}}>Rate your Booking!</Text>
                    
                    <View
                      style={{    
                        borderBottomWidth: 1.5,
                        margin: 10,
                        borderColor: "#E7E7E7",
                      }}
                    />

                    <View style ={{alignItems: 'center'}}>
                      <Text style= {{color: apptheme, fontFamily: 'Quicksand-Bold'}}> {this.props.salonName} </Text>

                      
                      <View style={{marginTop: 10}}>
                        <StarRating
                          disabled={false}
                          emptyStar={'ios-star-outline'}
                          fullStar={'ios-star'}
                          halfStar={'ios-star-half'}
                          iconSet={'Ionicons'}
                          maxStars={5}
                          rating={this.state.starCount}
                          starColor={'#EA0042'}
                          emptyStarColor= {'#EA0042'}
                          fullStarColor= {"#EA0042"}
                          starSize={25}
                          starStyle={{padding: 2}}
                          selectedStar={(rating) => this.onStarRatingPress(rating)} 


                          />        
                      </View>            

                      <TextInput style= {styles.commentview}
                        multiline={true}
                        autoCorrect= {false}
                        placeholder= {locale.t("postYourReviewsFor") + this.props.salonName +locale.t("toChangeItsRating")}
                        onChangeText= {(text)=> {this.userComments= text}}
                        underlineColorAndroid={transparent}
                        > 
                      </TextInput>
                    </View>
                      
                  </View>
                  
                  <View
                      style={{
                        width: "100%",
                        borderBottomWidth: 1.5,
                        borderColor: "#E7E7E7",
                      }}
                  />
                
                  <View style={{width: "100%",flexDirection: 'row', justifyContent: 'center',}}>
                  
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({ visible: false });
                        this.props.callback(false);
                      }}
                      style={{ width: "50%", alignItems: 'center', }}
                    >
                      <Text style={[styles.buttonstyle, {color: coralred} ]}>CANCEL</Text>
                    </TouchableOpacity>

                    <View
                      style={{
                        borderWidth: 1,
                        borderColor: "#E7E7E7",
                      }}
                  />

                    <TouchableOpacity
                      onPress={() => {
                        //this.setState({ visible: false });
                        this.props.rating(this.props.salonId, this.state.starCount, this.userComments);
                      }}
                      style={{  width: "50%", alignItems: 'center' }}
                    >
                      <Text style={[styles.buttonstyle, {color: apptheme}]}>RATE</Text>
                    </TouchableOpacity>

                  </View>
                
                </View>

              </Modal>
            </ScrollView>
          </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  bodyContainer: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: white,
    borderRadius: 10,
  },
  text: {
    alignItems: "center",
    paddingRight: 20,
    paddingLeft: 20,
    paddingTop: 20
  },
  heading: {
    fontSize: 16,
    fontFamily: "Quicksand-Bold",
    marginBottom: 10
  },
  body: {
    fontSize: 14,
    marginBottom: 10
  },
  buttonstyle: {
    fontWeight: "bold",
    fontSize: 16,
    marginTop: 10,
    marginBottom: 10,
  },
  commentview:{
    fontSize: 14,
    padding: 8,
    height: 64,
    backgroundColor: white,
    borderRadius: 5,
    marginBottom: 10,
    marginTop: 10,
    width: '100%'     
  },
});

export default RatingDialog;
