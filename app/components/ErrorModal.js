import React, { Component } from "react";

import { StyleSheet, TouchableOpacity, Alert, View } from "react-native";
import { locale } from "../containers/AppContainer";

export default class ErrorModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showError: props.show
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ showError: nextProps.show });
  }

  showAlert = () => {
    Alert.alert(
      this.props.alertMessage,
      "",
      [
        {
          text: locale.t('ok'),
          onPress: () => {
            this.props.onRequestClose();
          }
        }
      ],
      { cancelable: false }
    );
  };

  render() {
    return (
      <View>
        {this.state.showError ? <View>{this.showAlert()}</View> : <View />}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white"
  }
});
