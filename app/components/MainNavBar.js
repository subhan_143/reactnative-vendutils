import React, { Component, PropTypes } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Text,
  Alert,
  I18nManager
} from "react-native";

import { apptheme, white } from "../color";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/Ionicons";
import { cancelPreBooking } from "../sources/APIService";
import { locale } from "../containers/AppContainer";

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.openDrawer = this.openDrawer.bind(this);
    this.goBack = this.goBack.bind(this);
    this._Ref = null;
    this.state = {};
  }

  openDrawer() {
    this.props.toggleMenu();
  }

  goBack() {
    if (global.navigatedToSideMenuItem) {
      global.navigatedToSideMenuItem = false
    }
    if (this.props.onBackPress) {
      this.props.onBackPress();
    } else {
     
      Actions.pop();
      
    }
  }

  shouldCancel = id => {
    Alert.alert(
      "",
      locale.t(
        "theServicesRequiringPrepaymentWillBeRemovedFromYourBookingDoYouWishToContinue"
      ),
      [
        { text: locale.t("no"), onPress: () => {} },
        {
          text: locale.t("yes"),
          onPress: () => {
            this.cancelBooking(id);
          }
        }
      ],
      { cancelable: false }
    );
  };

  //TODO: Need to move this method in Payment page
  cancelBooking(id) {
    cancelPreBooking(id)
      .then(
        response => {
          var message = "";
          var title_message = "";
          if (response.isThank) {
            message =
              locale.t("allServiceThatDonotRequirePaymentBooked") +
              "\n\n" +
              locale.t("youWillBeRewardedOnePoint");
          } else {
            title_message = locale.t("weAreSorry");
            message = locale.t("bookingNotCompleteSincePaymentDeclined");
          }
          Actions.thankyou({ message, title_message });
        },
        error => {
          Alert.alert(locale.t("serviceNotCancelledTryAgain"), [
            { text: locale.t("ok") }
          ]);
        }
      )
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    return (
      <View style={styles.backgroundStyle}>
        <View style={styles.container}>
          {this.props.hideNavBar ? (
            <View style={styles.leftBarButton} />
          ) : (
            <TouchableOpacity
              style={styles.leftBarButton}
              onPress={this.goBack}
            >
              <Icon
                name="ios-arrow-back"
                size={24}
                color="#fff"
                style={{
                  transform: [{ scaleX: locale.locale === "arb" ? -1 : 1 }]
                }}
              />
            </TouchableOpacity>
          )}

          {this.props.routeName === "salondetail" ? (
            <Text style={styles.title}>{this.props.salon_name}</Text>
          ) : (
            <Text style={styles.title}>{this.props.title}</Text>
          )}

          {this.props.ispayment ? 
            <TouchableOpacity
              style={styles.rightBarButton}
              onPress={() => {
                this.shouldCancel(this.props.bookingid);
              }}
            >
              {/*<Icon name="ios-close-circle-outline" size={24} color={white} />*/}
              <Text style={styles.subtitle}>{locale.t("cancel")}</Text>
            </TouchableOpacity>
           :            
           
           this.props.rateSalon?
            <TouchableOpacity
              style={styles.rightBarButton}
              onPress={()=> {this.props.rateSalon()}}>
              <Icon name="ios-create-outline" size={30} color="#fff" />
            </TouchableOpacity>  
           :
           this.props.hideHamBurger ? 
            <View style={styles.rightBarButton} />
           : 
            <TouchableOpacity
              style={styles.rightBarButton}
              onPress={this.openDrawer}
            >
              <Icon name="ios-menu" size={24} color="#fff" />
            </TouchableOpacity>
          }
        </View>
      </View>
    );
  }
}

NavBar.propTypes = {
  title: PropTypes.string
};

const styles = {
  backgroundStyle: {
    backgroundColor: apptheme
  },
  container: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginTop: Math.floor(Dimensions.get("window").height) === 812 ? 40 : 20,
    height: 44
  },
  leftBarButton: {
    width: 68,
    marginLeft: 8,
    padding: 8,
    alignItems: "flex-start",
    marginRight: "auto"
  },
  rightBarButton: {
    width: 68,
    marginRight: 8,
    padding: 8,
    alignItems: "flex-end",
    marginLeft: "auto"
  },
  title: {
    color: white,
    fontSize: 18,
    alignSelf: "center",
    fontFamily: "Quicksand-Bold"
  },
  subtitle: {
    color: white, 
    fontSize: 13, 
    fontWeight: "500"
  }
};

export default NavBar;
