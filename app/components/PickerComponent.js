import React, { Component, PropTypes } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  Text,
  Picker,
  Platform,
  TextInput,
  Alert
} from "react-native";
import { apptheme, white, lightgray } from "../color";
import Icon from "react-native-vector-icons/Ionicons";
import DrawerContent from "../containers/DrawerContent";
import styles from "../assets/styles/PickerComponentStyles";
import SimplePicker from "react-native-simple-picker";
import { locale } from "../containers/AppContainer";

const options = ["Option1", "Option2", "Option3"];

class PickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: locale.t("country"),
      phoneCode: "+ ",
      number: "",
      placeholderColor: lightgray,
      pickerPlaceHolderColor: apptheme,
      countrySelected: false
    };
    this.numberCode = "";
    this.countryId = "";
    this.list = [];
    this.getItems = this.getItems.bind(this);

    this.getCountryName();

  }

  setCountrycode(country) {
    this.props.fetchedCountries.map(data => {
      if (data.name === country) {
        this.numberCode = data.code;
        this.countryId = data.id;
      }
    });
  }

  updateCountryFunc(country, i) {
    // depending on country different codes will be assigned
    this.setState({countrySelected : true})
    if (Platform.OS === "ios") {
      this.setCountrycode(country);
    } else {
      if(!this.state.countrySelected ){
        this.countryId = this.props.fetchedCountries[i - 1].id;
        this.numberCode = this.props.fetchedCountries[i - 1].code;
      }
      else {
        this.countryId = this.props.fetchedCountries[i].id;
        this.numberCode = this.props.fetchedCountries[i].code;
      }
    }
    this.setState({
      country: country,
      phoneCode: this.numberCode,
      placeholderColor: "#000000"
    });
    this.props.updateCountryCodeFunc(country, this.numberCode, this.countryId);
  }

  updateNumber(number) {
    this.setState({ number: number });
    this.props.updatePhoneNumber(number);
  }

  getItems() {
    let items = this.props.fetchedCountries.map((data, i) => {
      return <Picker.Item label={data.name} value={data.name} key={i} />;
    });

    if (!this.state.countrySelected) {
      let placeHolderItem = <Picker.Item label={locale.t("selectCountryPlaceHolder")} value="0" />
      items.unshift(placeHolderItem) 
    }

    return items
  }

  getCountryName() {
    return this.props.fetchedCountries.map((data, i) => {
      return this.list.push(data.name);
    });
  }

  render() {
    //console.log("Picker: ",pickerData);
    return (
      <View style={styles.container}>
        {Platform.OS === "ios" ? (
          <View style={styles.pickerStyle}>
            <Text
              style={[
                styles.countrytext,
                { color: this.state.placeholderColor }
              ]}
              onPress={() => {
                this.refs.picker2.show();
              }}
            >
              { this.state.countrySelected ? this.state.country : locale.t("selectCountryPlaceHolder")} 
            </Text>

            <SimplePicker
              ref={"picker2"}
              options={this.list}
              cancelText={"Done"}
              buttonStyle={{fontSize: 14, fontWeight: "500"}}
              confirmText={" "}
              onSubmit={option => {
                this.updateCountryFunc(option);
              }}
            />
          </View>
        ) : (
          <View style={styles.pickerStyle}>
            <Picker

                mode={"dialog"}
                placeholder={locale.t("selectCountryPlaceHolder")}  
              selectedValue={this.state.country}
              onValueChange={(country, i) => this.updateCountryFunc(country, i)}
            >
              {this.getItems()}
            </Picker>
          </View>
        )}

        <View style={styles.phoneSection}>
          <View style={styles.regionSection}>
            <View style={styles.textSection}>
              <Text
                style={[
                  styles.regionInput,
                  { color: this.state.placeholderColor }
                ]}
              >
                {this.state.phoneCode}
              </Text>
            </View>
          </View>
          <View style={styles.phoneInputSection}>
            {Platform.OS === "ios" ? (
              <TextInput
                style={styles.phoneInput}
                placeholder={locale.t("phoneNumber")}
                placeholderTextColor={this.state.placeholderColor}
                keyboardType="numeric"
                value={this.state.number}
                onChangeText={number => this.updateNumber(number)}
              />
            ) : (
              <TextInput
                style={styles.phoneInput}
                placeholder={locale.t("phoneNumber")}
                placeholderTextColor={this.state.placeholderColor}
                keyboardType="numeric"
                value={this.state.number}
                underlineColorAndroid={"transparent"}
                onChangeText={number => this.updateNumber(number)}
              />
            )}
          </View>
        </View>
      </View>
    );
  }
}

export default PickerComponent;
