/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import AppContainer from './containers/AppContainer';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise';
import allReducers from './reducers';
import { apptheme } from './color';


// middleware that logs actions
const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__  });

const store = createStore(
  allReducers,
  applyMiddleware(thunkMiddleware, promise, loggerMiddleware)
);

const App = () => (
    <Provider store={store}>
      <AppContainer />
    </Provider>
);

export default App;


// registerScreens(store, Provider);

//   // start the app
//   Navigation.startSingleScreenApp({
//     screen: {
//       screen: 'ParentPage', // this is a registered name for a screen
//     },
//     drawer: {
//       right: {
//         screen: 'DrawerContent',
//       },
//       style: { // ( iOS only )
//         drawerShadow: true, // optional, add this if you want a side menu drawer shadow
//         contentOverlayColor: 'rgba(0,0,0,0.25)', // optional, add this if you want a overlay when drawer is open
//       },
//       disableOpenGesture: true
//     },
//   });
